# mac修改host脚本

#### 介绍
python脚本便捷操作mac hosts文件

#### 软件架构
支持python2.7 3.x版本的自己修改不能用的函数


#### 安装教程

1.  下载文件后,cd 到下载目录  sudo python hosts.py 按照提示操作
2.  编辑 vim ~/.zshrc  新加一行 alias hosts='sudo python /存放本文件的目录/hosts.py'  执行命令使修改生效 source ~/.zshrc
3.  编辑 vim ~/.bash_profile  新加一行 function hosts(){ sudo python /存放本文件的目录/hosts.py } 加上执行权限 chmod +x ~/.bash_profile 使命令马上生效 source ~/.bash_profile

4. 使用 23方案 直接在终端输入hosts即可调用

#### 使用说明

1.  随手写的看量升级,初学python不是很精通
2.  输入没做验证,执行没做验证,操作之前最好先备份文件


